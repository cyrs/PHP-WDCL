<?php
namespace Ths\Web\Tests\Util;
use Ths\Web\Tests\Test\BaseTest;
use Ths\Web\Util\StringUtils;

class StringUtilsTest extends BaseTest
{
    public function testNullToEmpty()
    {
         $this->assertSame('', StringUtils::nullToEmpty(null));
         $this->assertSame('', StringUtils::nullToEmpty(''));
         $this->assertSame(' ', StringUtils::nullToEmpty(' '));
         $this->assertSame('abc', StringUtils::nullToEmpty('abc'));
    }

    public function testIsEmpty()
    {
        $this->assertTrue(StringUtils::isEmpty(null));
        $this->assertTrue(StringUtils::isEmpty(''));
        $this->assertFalse(StringUtils::isEmpty(' '));
        $this->assertFalse(StringUtils::isEmpty('bob'));
        $this->assertFalse(StringUtils::isEmpty(' bob '));
    }

    public function testIsNotEmpty()
    {
        $this->assertFalse(StringUtils::isNotEmpty(null));
        $this->assertFalse(StringUtils::isNotEmpty(''));
        $this->assertTrue(StringUtils::isNotEmpty(' '));
        $this->assertTrue(StringUtils::isNotEmpty('bob'));
        $this->assertTrue(StringUtils::isNotEmpty('  bob  '));
    }

    public function testIsBlank()
    {
        $this->assertTrue(StringUtils::isBlank(null));
        $this->assertTrue(StringUtils::isBlank(''));
        $this->assertTrue(StringUtils::isBlank(' '));
        $this->assertFalse(StringUtils::isBlank('bob'));
        $this->assertFalse(StringUtils::isBlank('  bob  '));
    }

    public function testIsNotBlank()
    {
        $this->assertFalse(StringUtils::isNotBlank(null));
        $this->assertFalse(StringUtils::isNotBlank(''));
        $this->assertFalse(StringUtils::isNotBlank(' '));
        $this->assertTrue(StringUtils::isNotBlank('bob'));
        $this->assertTrue(StringUtils::isNotBlank('  bob  '));
    }

    public function testLowerCase()
    {
        $this->assertNull(StringUtils::lowerCase(null));
        $this->assertEquals('', StringUtils::lowerCase(''));
        $this->assertSame('abc', StringUtils::lowerCase('aBc'));
    }

    public function testUpperCase()
    {
        $this->assertNull(StringUtils::upperCase(null));
        $this->assertEquals('', StringUtils::upperCase(''));
        $this->assertSame('ABC', StringUtils::upperCase('aBc'));
    }

    public function testStartsWith()
    {
        $this->assertTrue(StringUtils::startsWith(null, null));
        $this->assertFalse(StringUtils::startsWith(null, 'abc'));
        $this->assertFalse(StringUtils::startsWith('abcdef', null));
        $this->assertTrue(StringUtils::startsWith('', ''));
        $this->assertTrue(StringUtils::startsWith('abcdef', ''));
        $this->assertTrue(StringUtils::startsWith('abcdef', 'abc'));
        $this->assertFalse(StringUtils::startsWith('ABCDEF', 'abc'));
        $this->assertFalse(StringUtils::startsWith('abcdef', 'abd'));
    }

    public function testStartsWithIgnoreCase()
    {
        $this->assertTrue(StringUtils::startsWithIgnoreCase(null, null));
        $this->assertFalse(StringUtils::startsWithIgnoreCase(null, 'abc'));
        $this->assertFalse(StringUtils::startsWithIgnoreCase('abcdef', null));
        $this->assertTrue(StringUtils::startsWithIgnoreCase('', ''));
        $this->assertTrue(StringUtils::startsWithIgnoreCase('abcdef', ''));
        $this->assertTrue(StringUtils::startsWithIgnoreCase('abcdef', 'abc'));
        $this->assertTrue(StringUtils::startsWithIgnoreCase('ABCDEF', 'abc'));
        $this->assertFalse(StringUtils::startsWithIgnoreCase('abcdef', 'abd'));
    }

    public function testEndsWith()
    {
        $this->assertTrue(StringUtils::endsWith(null, null));
        $this->assertFalse(StringUtils::endsWith(null, 'def'));
        $this->assertFalse(StringUtils::endsWith('abcdef', null));
        $this->assertTrue(StringUtils::endsWith('abcdef', 'def'));
        $this->assertTrue(StringUtils::endsWith('abcdef', ''));
        $this->assertFalse(StringUtils::endsWith('ABCDEF', 'def'));
        $this->assertFalse(StringUtils::endsWith('ABCDEF', 'cde'));
    }

    public function testEndsWithIgnoreCase()
    {
        $this->assertTrue(StringUtils::endsWithIgnoreCase(null, null));
        $this->assertFalse(StringUtils::endsWithIgnoreCase(null, 'def'));
        $this->assertFalse(StringUtils::endsWithIgnoreCase('abcdef', null));
        $this->assertTrue(StringUtils::endsWithIgnoreCase('abcdef', ''));
        $this->assertTrue(StringUtils::endsWithIgnoreCase('abcdef', 'def'));
        $this->assertTrue(StringUtils::endsWithIgnoreCase('ABCDEF', 'def'));
        $this->assertFalse(StringUtils::endsWithIgnoreCase('ABCDEF', 'cde'));
    }

    public function testContains()
    {
        $this->assertFalse(StringUtils::contains(null, '*'));
        $this->assertFalse(StringUtils::contains('*', null));
        $this->assertTrue(StringUtils::contains('', ''));
        $this->assertTrue(StringUtils::contains('abc', ''));
        $this->assertFalse(StringUtils::contains('', 'a'));
        $this->assertTrue(StringUtils::contains('abc', 'a'));
        $this->assertFalse(StringUtils::contains('abc', 'z'));
        $this->assertFalse(StringUtils::contains('ABC', 'a'));
    }

    public function testSubstringBefore()
    {
        $this->assertNull(StringUtils::substringBefore(null, '*'));
        $this->assertSame('', StringUtils::substringBefore('', '*'));
        $this->assertSame('', StringUtils::substringBefore('abc', null));
        $this->assertSame('', StringUtils::substringBefore('abc', ''));
        $this->assertSame('', StringUtils::substringBefore('abc', 'd'));
        $this->assertSame('', StringUtils::substringBefore('abc', 'a'));
        $this->assertSame('a', StringUtils::substringBefore('abcba', 'b'));
        $this->assertSame('ab', StringUtils::substringBefore('abc', 'c'));
    }

    public function testSubstringAfter()
    {
        $this->assertNull(StringUtils::substringAfter(null, '*'));
        $this->assertSame('', StringUtils::substringAfter('', '*'));
        $this->assertSame('', StringUtils::substringAfter('abc', null));
        $this->assertSame('', StringUtils::substringAfter('abc', ''));
        $this->assertSame('', StringUtils::substringAfter('abc', 'd'));
        $this->assertSame('', StringUtils::substringAfter('abc', 'c'));
        $this->assertSame('cba', StringUtils::substringAfter('abcba', 'b'));
        $this->assertSame('bc', StringUtils::substringAfter('abc', 'a'));
    }
}

