<?php
namespace Ths\Web\Tests\Test;

use PHPUnit_Framework_TestCase;

/**
 * 测试基类
 *
 * @author tianhs
 *
 */
class BaseTest extends PHPUnit_Framework_TestCase
{

    protected function setUp()
    {
        parent::setUp();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

}

