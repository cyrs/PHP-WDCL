<?php
use PHPUnit_Framework_TestSuite;

class UtilTestSuite extends PHPUnit_Framework_TestSuite {

    public function __construct(){
        $this->addTestFile('./tests/Util/StringUtilsTest.php');
    }

    public static function suite() {
        return new self();
    }
}
?>E